set -ex
COMMIT_HASH=$1
NODE_ROLE=$2
BUILD_TYPE=$3
BINDIR=`dirname $0`
source $BINDIR/common.sh

if [ -f $SRCDIR/oai-setup-complete ]; then
    echo "setup already ran; not running again"
    exit 0
fi

function setup_cn_node {
    # do stuff
    echo setting up cn
}

function setup_ran_node {
    cd $SRCDIR
    git clone $OAI_RAN_MIRROR oairan
    cd oairan
    git checkout $COMMIT_HASH
    source oaienv
    cd cmake_targets
    ./build_oai -I
    ./build_oai -w USRP $BUILD_ARGS
}

if [ $NODE_ROLE == "cn" ]; then
    BUILD_ARGS=""
    setup_cn_node
elif [ $NODE_ROLE == "nodeb" ]; then
    if [ $BUILD_TYPE == "lte" ]; then
        BUILD_ARGS="--eNB"
    else
        BUILD_ARGS="--gNB"
    fi
    setup_ran_node
elif [ $NODE_ROLE == "ue" ]; then
    if [ $BUILD_TYPE == "lte" ]; then
        BUILD_ARGS="--UE"
    else
        BUILD_ARGS="--nrUE"
    fi
    setup_ran_node
fi

touch $SRCDIR/oai-setup-complete
