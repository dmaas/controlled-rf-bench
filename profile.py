#!/usr/bin/env python

import os
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.igext as IG
import geni.rspec.emulab.pnext as PN


tourDescription = """
### Controlled RF Bench

This profile instantiates an experiment that includes resources from one of our
controlled RF test benches, with options to provision the associated compute
resources for the following open source LTE/5G tests:

1. OAI 5G SA mode w/out a core network (OAI gNB/UE, band n78, TDD)
2. srsRAN 4G E2E (SRS EPC/eNB/UE, band 7, FDD)

The controlled RF benches currently include two USRP X310s, each with a single
UBX160 daughterboard. An Octoclock provides a common 10 MHz clock and PPS
reference. The transceivers are connected via SMA cables through 30 dB
attenuators, providing for an interference free RF environment. `bench_c` is for
internal use only, so select `bench_a` or `bench_b` if you are not a Powder team
member.


"""

tourInstructions = """

Depending on the configuration of your experiment, startup scripts may still be
running when after your experiment becomes ready. Watch the "Startup" column on
the "List View" tab for your experiment and wait until all of the compute nodes
show "Finished" before proceeding.

#### OAI 5G SA w/out Core Network

On `nodeb`:

```
sudo /var/tmp/oairan/cmake_targets/ran_build/build/nr-softmodem -E \
  -O /local/repository/etc/gnb.sa.band78.fr1.106PRB.usrpx310.conf --sa

```

On `ue`:

```
sudo /var/tmp/oairan/cmake_targets/ran_build/build/nr-uesoftmodem -E \
  --sa --usrp-args "clock_source=external,type=x300"

```

The UE will decode SIB1, initiate the random access procedure, and eventually
reach the RRCConnected state. After this, the gNB and UE will start printing
some HARQ round statistics for the downlink and uplink shared channels.

Known issues:

- Exiting the scripts via ctrl-c will often leave the SDR in a funny state, so
  that the next time you start the nodeB/UE, it may crash. If this happens,
  simply start the nodeB/UE again.

- The UE may hang after ctrl-c in some cases, requiring you to kill it some
  other way. If this happens, us `ps aux` to identify the PID of of the
  softmodem process and `kill -9 {PID}` to kill it.

#### srsRAN E2E

In one session on `nodeb`:

```
sudo srsepc

```

In a second session on `nodeb`:

```
sudo srsenb

```

On `ue`:

```
sudo srsue

```

The srs UE will sync with the eNB. Entering `t` into stdin on either process
will start printing various LTE metrics to stdout.

Additionaly, you can use the tun interfaces to test the uplink/downlink with
ping or iperf.

"""

BIN_PATH = "/local/repository/bin"
ETC_PATH = "/local/repository/etc"
SRSRAN_IMG = "urn:publicid:IDN+emulab.net+image+PowderTeam:U18LL-SRSLTE"
DOCKER_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//docker-ubuntu18-std"
UBUNTU_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
COMP_MANAGER_ID = "urn:publicid:IDN+emulab.net+authority+cm"
DEFAULT_NR_RAN_HASH = "c3c925bd4d8c77727451e0dabca280b01ce9796d"
DEFAULT_LTE_RAN_HASH = "3dc9e3d19c0faacfa47cd0b425d51eb9ad60a2bf"
DEFAULT_NR_CN_HASH = "d1514cbfc9f85f570d73d6ebed362f480ff2f369"
DEFAULT_LTE_CN_HASH = "e8fefda8f6e182006ec677170ce3eab3da261032"
SETUP_SCRIPT = os.path.join(BIN_PATH, "deploy-oai.sh")


BENCH_SDR_IDS = {
    "bench_a": ["oai-wb-a1", "oai-wb-a2"],
    "bench_b": ["oai-wb-b1", "oai-wb-b2"],
    "bench_c": ["alex-3", "alex-4"],
}

pc = portal.Context()

node_types = [
    ("d740", "Emulab, d740"),
    ("d430", "Emulab, d430"),
]
pc.defineParameter(
    name="sdr_nodetype",
    description="Type of compute node paired with the SDRs",
    typ=portal.ParameterType.STRING,
    defaultValue=node_types[0],
    legalValues=node_types
)

bench_ids = [
    ("bench_a", "Controlled RF bench A"),
    ("bench_b", "Controlled RF bench B"),
    ("bench_c", "Controlled RF bench C (Powder staff only)"),
]
pc.defineParameter(
    name="bench_id",
    description="Which controlled RF test bench to use",
    typ=portal.ParameterType.STRING,
    defaultValue=bench_ids[0],
    legalValues=bench_ids
)

pc.defineParameter(
    name="deploy_nr",
    description="Deploy 5GNR RAN/CN components",
    typ=portal.ParameterType.BOOLEAN,
    defaultValue=True,
    advanced=True
)

pc.defineParameter(
    name="oai_ran_commit_hash",
    description="Commit hash for OAI RAN",
    typ=portal.ParameterType.STRING,
    defaultValue="",
    advanced=True
)

pc.defineParameter(
    name="include_cn",
    description="Include a node for the OAI CN",
    typ=portal.ParameterType.BOOLEAN,
    defaultValue=False,
    advanced=True
)

pc.defineParameter(
    name="cn_nodetype",
    description="Type of compute node to use for CN node (if included)",
    typ=portal.ParameterType.STRING,
    defaultValue=node_types[0],
    legalValues=node_types,
    advanced=True
)

# pc.defineParameter(
#     name="oai_cn_commit_hash",
#     description="Commit hash for OAI (5G)CN",
#     typ=portal.ParameterType.STRING,
#     defaultValue="",
#     advanced=True
# )

pc.defineParameter(
    name="sdr_compute_image",
    description="Image to use for compute connected to SDRs",
    typ=portal.ParameterType.STRING,
    defaultValue=SRSRAN_IMG,
    advanced=True
)

pc.defineParameter(
    name="cn_compute_image",
    description="Image to use for CN node (if included)",
    typ=portal.ParameterType.STRING,
    defaultValue=UBUNTU_IMG,
    advanced=True
)

params = pc.bindParameters()
request = pc.makeRequestRSpec()

network_type = "nr" if params.deploy_nr else "lte"
if params.include_cn:
    role = "cn"
    cn_node = request.RawPC(role)
    cn_node.component_manager_id = COMP_MANAGER_ID
    cn_node.hardware_type = params.cn_nodetype
    cn_node.disk_image = params.cn_compute_image
    cn_if = cn_node.addInterface("cn-if")
    cn_if.addAddress(rspec.IPv4Address("192.168.1.1", "255.255.255.0"))
    cn_link = request.Link("cn-link")
    cn_link.bandwidth = 10*1000*1000
    cn_link.addInterface(cn_if)
    # if params.oai_cn_commit_hash:
    #     cn_hash = params.oai_cn_commit_hash
    # else:
    #     cn_hash = DEFAULT_NR_CN_HASH if params.deploy_nr else DEFAULT_LTE_CN_HASH

    # cmd = "{} {} {} {}".format(SETUP_SCRIPT, cn_hash, role, network_type)

    # cn_node.addService(rspec.Execute(shell="bash", command=cmd))

if params.oai_ran_commit_hash:
    ran_hash = params.oai_ran_commit_hash
else:
    ran_hash = DEFAULT_NR_RAN_HASH if params.deploy_nr else DEFAULT_LTE_RAN_HASH

role = "nodeb"
nodeb = request.RawPC(role)
nodeb.component_manager_id = COMP_MANAGER_ID
nodeb.hardware_type = params.sdr_nodetype
nodeb.disk_image = params.sdr_compute_image
if params.include_cn:
    nodeb_cn_if = nodeb.addInterface("nodeb-cn-if")
    nodeb_cn_if.addAddress(rspec.IPv4Address("192.168.1.2", "255.255.255.0"))
    cn_link.addInterface(nodeb_cn_if)

nodeb_usrp_if = nodeb.addInterface("nodeb-usrp-if")
nodeb_usrp_if.addAddress(rspec.IPv4Address("192.168.40.1", "255.255.255.0"))
cmd = "{} {} {} {}".format(SETUP_SCRIPT, ran_hash, role, network_type)
nodeb.addService(rspec.Execute(shell="bash", command=cmd))
nodeb.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-cpu.sh"))
nodeb.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-sdr-iface.sh"))

nodeb_sdr = request.RawPC("nodeb-sdr")
nodeb_sdr.component_manager_id = COMP_MANAGER_ID
nodeb_sdr.component_id = BENCH_SDR_IDS[params.bench_id][0]
nodeb_sdr_if = nodeb_sdr.addInterface("nodeb-sdr-if")

nodeb_sdr_link = request.Link("nodeb-sdr-link")
nodeb_sdr_link.bandwidth = 10*1000*1000
nodeb_sdr_link.addInterface(nodeb_usrp_if)
nodeb_sdr_link.addInterface(nodeb_sdr_if)

role = "ue"
ue = request.RawPC(role)
ue.component_manager_id = COMP_MANAGER_ID
ue.hardware_type = params.sdr_nodetype
ue.disk_image = params.sdr_compute_image
ue_usrp_if = ue.addInterface("ue-usrp-if")
ue_usrp_if.addAddress(rspec.IPv4Address("192.168.40.1", "255.255.255.0"))
cmd = "{} {} {} {}".format(SETUP_SCRIPT, ran_hash, role, network_type)
ue.addService(rspec.Execute(shell="bash", command=cmd))
ue.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-cpu.sh"))
ue.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-sdr-iface.sh"))

ue_sdr = request.RawPC("ue-sdr")
ue_sdr.component_manager_id = COMP_MANAGER_ID
ue_sdr.component_id = BENCH_SDR_IDS[params.bench_id][1]
ue_sdr_if = ue_sdr.addInterface("ue-sdr-if")

ue_sdr_link = request.Link("ue-sdr-link")
ue_sdr_link.bandwidth = 10*1000*1000
ue_sdr_link.addInterface(ue_usrp_if)
ue_sdr_link.addInterface(ue_sdr_if)

tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN, tourDescription)
tour.Instructions(IG.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

pc.printRequestRSpec(request)
